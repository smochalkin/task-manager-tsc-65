package ru.smochalkin.tm.api;

import ru.smochalkin.tm.model.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {
}
