package ru.smochalkin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.model.IHasEndDate;

import java.util.Comparator;

public final class ComparatorByEndDate implements Comparator<IHasEndDate> {

    @NotNull
    public final static ComparatorByEndDate INSTANCE = new ComparatorByEndDate();

    @NotNull
    private ComparatorByEndDate() {
    }

    @Override
    public int compare(@NotNull IHasEndDate o1, @NotNull IHasEndDate o2) {
        if (o1.getEndDate() == null) return 1;
        if (o2.getEndDate() == null) return -1;
        return o1.getEndDate().compareTo(o2.getEndDate());
    }

}
