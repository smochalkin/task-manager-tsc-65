package ru.smochalkin.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.smochalkin.tm.endpoint.*;

@Configuration
@ComponentScan("ru.smochalkin.tm")
public class ContextConfiguration {

    @Bean
    @NotNull
    public AdminEndpoint adminEndpoint() {
        return new AdminEndpointService().getAdminEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

}
