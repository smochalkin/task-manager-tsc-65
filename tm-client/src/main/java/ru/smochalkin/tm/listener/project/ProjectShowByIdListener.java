package ru.smochalkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractProjectListener;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String name() {
        return "project-show-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by id.";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDto project = projectEndpoint.findProjectById(sessionService.getSession(), id);
        showProject(project);
    }

}
