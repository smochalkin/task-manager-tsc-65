package ru.smochalkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.listener.AbstractSystemListener;

import java.util.List;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String arg() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of commands.";
    }

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@commandListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.name() != null) {
                System.out.println(listener);
            }
        }
    }

}
