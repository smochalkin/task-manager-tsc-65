package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public class UserUnlockByLoginListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User unlocking by login.";
    }

    @Override
    @EventListener(condition = "@userUnlockByLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final Result result = adminEndpoint.unlockUserByLogin(sessionService.getSession(), login);
        printResult(result);
    }

}
