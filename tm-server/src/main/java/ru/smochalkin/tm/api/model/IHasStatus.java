package ru.smochalkin.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable final Status status);

}
