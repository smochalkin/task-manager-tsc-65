package ru.smochalkin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.model.IHasCreated;

import java.util.Comparator;

public final class ComparatorByCreated implements Comparator<IHasCreated> {

    @NotNull
    public final static ComparatorByCreated INSTANCE = new ComparatorByCreated();

    @NotNull
    private ComparatorByCreated() {
    }

    @Override
    public int compare(@NotNull final IHasCreated o1, @NotNull final IHasCreated o2) {
        if (o1.getCreated() == null) return 1;
        if (o2.getCreated() == null) return -1;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
