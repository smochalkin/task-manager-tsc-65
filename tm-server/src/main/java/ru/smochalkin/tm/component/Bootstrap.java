package ru.smochalkin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.endpoint.*;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.service.dto.*;
import ru.smochalkin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public final class Bootstrap implements ServiceLocator {

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private  IProjectTaskService projectTaskService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    public IUserService userService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void init() {
        initPID();
        initEndpoints();
        initJMS();
    }

    private void initEndpoints(){
        Arrays.stream(endpoints).forEach(this::registryEndpoint);
    }

    private void registryEndpoint(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-server.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initJMS() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":" + BrokerService.DEFAULT_PORT;
        broker.addConnector(bindAddress);
        broker.start();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO SERVER TASK MANAGER **");
        init();
    }

}
