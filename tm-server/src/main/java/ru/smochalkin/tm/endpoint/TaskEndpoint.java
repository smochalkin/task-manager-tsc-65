package ru.smochalkin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.endpoint.ITaskEndpoint;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @Override
    @WebMethod
    @SneakyThrows
    public Result createTask(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.create(sessionDto.getUserId(), name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final String status
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.updateStatusById(sessionDto.getUserId(), id, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final String status
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.updateStatusByIndex(sessionDto.getUserId(), index, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeTaskStatusByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final String status
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.updateStatusByName(sessionDto.getUserId(), name, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDto> findTaskAllSorted(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "sort") @NotNull final String strSort
    ) {
        sessionService.validate(sessionDto);
        return taskService.findAll(sessionDto.getUserId(), strSort);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDto> findTaskAll(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        sessionService.validate(sessionDto);
        return taskService.findAll(sessionDto.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public TaskDto findTaskById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        sessionService.validate(sessionDto);
        return taskService.findById(sessionDto.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public TaskDto findTaskByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        sessionService.validate(sessionDto);
        return taskService.findByName(sessionDto.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public TaskDto findTaskByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        sessionService.validate(sessionDto);
        return taskService.findByIndex(sessionDto.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.removeById(sessionDto.getUserId(), id);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.removeByName(sessionDto.getUserId(), name);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeTaskByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.removeByIndex(sessionDto.getUserId(), index);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result clearTasks(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.clear(sessionDto.getUserId());
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateTaskById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.updateById(sessionDto.getUserId(), id, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateTaskByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        sessionService.validate(sessionDto);
        try {
            taskService.updateByIndex(sessionDto.getUserId(), index, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) {
        sessionService.validate(sessionDto);
        return projectTaskService.findTasksByProjectId(sessionDto.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result bindTaskByProjectId(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) {
        sessionService.validate(sessionDto);
        try {
            projectTaskService.bindTaskByProjectId(sessionDto.getUserId(), projectId, taskId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result unbindTaskByProjectId(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) {
        sessionService.validate(sessionDto);
        try {
            projectTaskService.unbindTaskByProjectId(sessionDto.getUserId(), projectId, taskId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
