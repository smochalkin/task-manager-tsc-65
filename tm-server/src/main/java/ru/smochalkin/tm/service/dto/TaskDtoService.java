package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.repository.dto.TaskRepository;

import java.util.*;
import java.util.stream.Collectors;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public class TaskDtoService extends AbstractDtoService<TaskDto> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @SneakyThrows
    public int getCount() {
        return (int) taskRepository.count();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        taskRepository.deleteByUserId(userId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll(@NotNull final String userId, @NotNull final String sort) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<TaskDto> comparator = sortType.getComparator();
        return taskRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findByName(@NotNull final String userId, @NotNull final String name) {
        return taskRepository.findFirstByUserIdAndName(userId, name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<TaskDto> list = taskRepository.findAllByUserId(userId);
        if (list.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId).get(index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDto task = findByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        taskRepository.delete(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @Nullable final TaskDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(desc);
        taskRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {

        @Nullable final TaskDto entity = findByIndex(userId, index);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(desc);
        taskRepository.save(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        @Nullable final TaskDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        taskRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        @Nullable final TaskDto project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        taskRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        @Nullable final TaskDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        taskRepository.save(project);
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        return index >= taskRepository.countByUserId(userId);
    }

    @SneakyThrows
    private Map<String, Date> prepareDates(@NotNull final Status status) {
        Map<String, Date> map = new HashMap<>();
        switch (status) {
            case IN_PROGRESS:
                map.put("start_date", new Date());
                map.put("end_date", null);
                break;
            case COMPLETED:
                map.put("start_date", null);
                map.put("end_date", new Date());
                break;
            case NOT_STARTED:
                map.put("start_date", null);
                map.put("end_date", null);
        }
        return map;
    }

}
