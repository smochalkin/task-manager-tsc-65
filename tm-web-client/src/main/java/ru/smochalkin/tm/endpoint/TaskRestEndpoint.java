package ru.smochalkin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.smochalkin.tm.api.ITaskRestEndpoint;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody List<Task> tasks) {
        repository.saveAll(tasks);
        return tasks;
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/saveAll")
    public List<Task> saveAll(@RequestBody List<Task> tasks) {
        repository.saveAll(tasks);
        return tasks;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll() {
        repository.removeAll();
    }

}
