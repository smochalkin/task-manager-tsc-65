package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.smochalkin.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date endDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date startDate;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

}