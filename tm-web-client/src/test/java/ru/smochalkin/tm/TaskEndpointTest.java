package ru.smochalkin.tm;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.smochalkin.tm.client.TaskRestEndpointClient;
import ru.smochalkin.tm.marker.RestCategory;
import ru.smochalkin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskEndpointTest {

    final Task task1 = new Task("test task 1");

    final Task task2 = new Task("test task 2");

    final TaskRestEndpointClient client = new TaskRestEndpointClient();

//    @BeforeClass
//    public static void beforeClass() {
//        new TaskRestEndpointClient().deleteAll();
//    }

    @Before
    public void before() {
        client.create(task1);
    }

    @After
    public void after() {
        client.deleteAll();
    }

    @Test
    @Category(RestCategory.class)
    public void find() {
        Assert.assertEquals(task1.getName(), client.find(task1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void create() {
        Assert.assertNotNull(client.create(task1));
    }

    @Test
    @Category(RestCategory.class)
    public void update() {
        final Task updatedTask = client.find(task1.getId());
        updatedTask.setName("updated");
        client.save(updatedTask);
        Assert.assertEquals("updated", client.find(task1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void delete() {
        client.delete(task1.getId());
        Assert.assertNull(client.find(task1.getId()));
    }

    @Test
    @Category(RestCategory.class)
    public void findAll() {
        Assert.assertEquals(1, client.findAll().size());
        client.create(task2);
        Assert.assertEquals(2, client.findAll().size());
    }

    @Test
    @Category(RestCategory.class)
    public void updateAll() {
        client.create(task2);
        final Task updatedTask = client.find(task1.getId());
        updatedTask.setName("updated1");
        final Task updatedTask2 = client.find(task2.getId());
        updatedTask2.setName("updated2");
        List<Task> updatedList = new ArrayList<>();
        updatedList.add(updatedTask);
        updatedList.add(updatedTask2);
        client.saveAll(updatedList);
        Assert.assertEquals("updated1", client.find(task1.getId()).getName());
        Assert.assertEquals("updated2", client.find(task2.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void createAll() {
        client.delete(task1.getId());
        List<Task> list = new ArrayList<>();
        list.add(task1);
        list.add(task2);
        client.createAll(list);
        Assert.assertEquals("test task 1", client.find(task1.getId()).getName());
        Assert.assertEquals("test task 2", client.find(task2.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void deleteAll() {
        client.create(task2);
        Assert.assertEquals(2, client.findAll().size());
        client.deleteAll();
        Assert.assertEquals(0, client.findAll().size());
    }

}
